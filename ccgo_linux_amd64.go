// Code generated for linux/amd64 by 'generator --libc modernc.org/libc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -o libpcre2.go --package-name libpcre2_posix .libs/libpcre2-posix.a -lpcre2-8', DO NOT EDIT.

//go:build linux && amd64
// +build linux,amd64

package libpcre2_posix

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libpcre2-8"
)

var (
	_ reflect.Type
	_ unsafe.Pointer
)

const m_COMPILE_ERROR_BASE = 100
const m_L_tmpnam = 20
const m_PCRE2_CASELESS = 8
const m_PCRE2_DOTALL = 32
const m_PCRE2_INFO_CAPTURECOUNT = 4
const m_PCRE2_LITERAL = 33554432
const m_PCRE2_MULTILINE = 1024
const m_PCRE2_NOTBOL = 1
const m_PCRE2_NOTEMPTY = 4
const m_PCRE2_NOTEOL = 2
const m_PCRE2_UCP = 131072
const m_PCRE2_UNGREEDY = 262144
const m_PCRE2_UTF = 524288
const m_REG_DOTALL = 16
const m_REG_ICASE = 1
const m_REG_NEWLINE = 2
const m_REG_NOSPEC = 4096
const m_REG_NOSUB = 32
const m_REG_NOTBOL = 4
const m_REG_NOTEMPTY = 256
const m_REG_NOTEOL = 8
const m_REG_PEND = 2048
const m_REG_STARTEND = 128
const m_REG_UCP = 1024
const m_REG_UNGREEDY = 512
const m_REG_UTF = 64
const m___FD_SETSIZE = 1024
const m___SIZEOF_PTHREAD_ATTR_T = 56
const m___SIZEOF_PTHREAD_BARRIERATTR_T = 4
const m___SIZEOF_PTHREAD_BARRIER_T = 32
const m___SIZEOF_PTHREAD_CONDATTR_T = 4
const m___SIZEOF_PTHREAD_COND_T = 48
const m___SIZEOF_PTHREAD_MUTEXATTR_T = 4
const m___SIZEOF_PTHREAD_MUTEX_T = 40
const m___SIZEOF_PTHREAD_RWLOCKATTR_T = 8
const m___SIZEOF_PTHREAD_RWLOCK_T = 56

type t__builtin_va_list = uintptr

type t__predefined_size_t = uint64

type t__predefined_wchar_t = int32

type t__predefined_ptrdiff_t = int64

type t__u_char = uint8

type t__u_short = uint16

type t__u_int = uint32

type t__u_long = uint64

type t__int8_t = int8

type t__uint8_t = uint8

type t__int16_t = int16

type t__uint16_t = uint16

type t__int32_t = int32

type t__uint32_t = uint32

type t__int64_t = int64

type t__uint64_t = uint64

type t__int_least8_t = int8

type t__uint_least8_t = uint8

type t__int_least16_t = int16

type t__uint_least16_t = uint16

type t__int_least32_t = int32

type t__uint_least32_t = uint32

type t__int_least64_t = int64

type t__uint_least64_t = uint64

type t__quad_t = int64

type t__u_quad_t = uint64

type t__intmax_t = int64

type t__uintmax_t = uint64

type t__dev_t = uint64

type t__uid_t = uint32

type t__gid_t = uint32

type t__ino_t = uint64

type t__ino64_t = uint64

type t__mode_t = uint32

type t__nlink_t = uint64

type t__off_t = int64

type t__off64_t = int64

type t__pid_t = int32

type t__fsid_t = struct {
	F__val [2]int32
}

type t__clock_t = int64

type t__rlim_t = uint64

type t__rlim64_t = uint64

type t__id_t = uint32

type t__time_t = int64

type t__useconds_t = uint32

type t__suseconds_t = int64

type t__suseconds64_t = int64

type t__daddr_t = int32

type t__key_t = int32

type t__clockid_t = int32

type t__timer_t = uintptr

type t__blksize_t = int64

type t__blkcnt_t = int64

type t__blkcnt64_t = int64

type t__fsblkcnt_t = uint64

type t__fsblkcnt64_t = uint64

type t__fsfilcnt_t = uint64

type t__fsfilcnt64_t = uint64

type t__fsword_t = int64

type t__ssize_t = int64

type t__syscall_slong_t = int64

type t__syscall_ulong_t = uint64

type t__loff_t = int64

type t__caddr_t = uintptr

type t__intptr_t = int64

type t__socklen_t = uint32

type t__sig_atomic_t = int32

const __ISupper = 256
const __ISlower = 512
const __ISalpha = 1024
const __ISdigit = 2048
const __ISxdigit = 4096
const __ISspace = 8192
const __ISprint = 16384
const __ISgraph = 32768
const __ISblank = 1
const __IScntrl = 2
const __ISpunct = 4
const __ISalnum = 8

type t__locale_struct = struct {
	F__locales       [13]uintptr
	F__ctype_b       uintptr
	F__ctype_tolower uintptr
	F__ctype_toupper uintptr
	F__names         [13]uintptr
}

type t__locale_t = uintptr

type Tlocale_t = uintptr

type Tptrdiff_t = int64

type Tsize_t = uint64

type Twchar_t = int32

type Tmax_align_t = struct {
	F__max_align_ll int64
	F__max_align_ld float64
}

type t__gnuc_va_list = uintptr

type t__mbstate_t = struct {
	F__count int32
	F__value struct {
		F__wchb [0][4]int8
		F__wch  uint32
	}
}

type t__fpos_t = struct {
	F__pos   t__off_t
	F__state t__mbstate_t
}

type T_G_fpos_t = t__fpos_t

type t__fpos64_t = struct {
	F__pos   t__off64_t
	F__state t__mbstate_t
}

type T_G_fpos64_t = t__fpos64_t

type T_IO_FILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     t__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         t__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [20]int8
}

type t__FILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     t__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         t__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [20]int8
}

type TFILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     t__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         t__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [20]int8
}

type T_IO_lock_t = struct{}

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

type Tva_list = uintptr

type Toff_t = int64

type Toff64_t = int64

type Tssize_t = int64

type Tfpos_t = struct {
	F__pos   t__off_t
	F__state t__mbstate_t
}

type Tfpos64_t = struct {
	F__pos   t__off64_t
	F__state t__mbstate_t
}

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tu_char = uint8

type Tu_short = uint16

type Tu_int = uint32

type Tu_long = uint64

type Tquad_t = int64

type Tu_quad_t = uint64

type Tfsid_t = struct {
	F__val [2]int32
}

type Tloff_t = int64

type Tino_t = uint64

type Tino64_t = uint64

type Tdev_t = uint64

type Tgid_t = uint32

type Tmode_t = uint32

type Tnlink_t = uint64

type Tuid_t = uint32

type Tpid_t = int32

type Tid_t = uint32

type Tdaddr_t = int32

type Tcaddr_t = uintptr

type Tkey_t = int32

type Tclock_t = int64

type Tclockid_t = int32

type Ttime_t = int64

type Ttimer_t = uintptr

type Tuseconds_t = uint32

type Tsuseconds_t = int64

type Tulong = uint64

type Tushort = uint16

type Tuint = uint32

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tu_int64_t = uint64

type Tregister_t = int32

type t__sigset_t = struct {
	F__val [16]uint64
}

type Tsigset_t = struct {
	F__val [16]uint64
}

type Ttimeval = struct {
	Ftv_sec  t__time_t
	Ftv_usec t__suseconds_t
}

type Ttimespec = struct {
	Ftv_sec  t__time_t
	Ftv_nsec t__syscall_slong_t
}

type t__fd_mask = int64

type Tfd_set = struct {
	Ffds_bits [16]t__fd_mask
}

type Tfd_mask = int64

type Tblksize_t = int64

type Tblkcnt_t = int64

type Tfsblkcnt_t = uint64

type Tfsfilcnt_t = uint64

type Tblkcnt64_t = int64

type Tfsblkcnt64_t = uint64

type Tfsfilcnt64_t = uint64

type t__atomic_wide_counter = struct {
	F__value32 [0]struct {
		F__low  uint32
		F__high uint32
	}
	F__value64 uint64
}

type t__pthread_list_t = struct {
	F__prev uintptr
	F__next uintptr
}

type t__pthread_internal_list = t__pthread_list_t

type t__pthread_slist_t = struct {
	F__next uintptr
}

type t__pthread_internal_slist = t__pthread_slist_t

type t__pthread_mutex_s = struct {
	F__lock    int32
	F__count   uint32
	F__owner   int32
	F__nusers  uint32
	F__kind    int32
	F__spins   int16
	F__elision int16
	F__list    t__pthread_list_t
}

type t__pthread_rwlock_arch_t = struct {
	F__readers       uint32
	F__writers       uint32
	F__wrphase_futex uint32
	F__writers_futex uint32
	F__pad3          uint32
	F__pad4          uint32
	F__cur_writer    int32
	F__shared        int32
	F__rwelision     int8
	F__pad1          [7]uint8
	F__pad2          uint64
	F__flags         uint32
	F__ccgo_pad12    [4]byte
}

type t__pthread_cond_s = struct {
	F__wseq         t__atomic_wide_counter
	F__g1_start     t__atomic_wide_counter
	F__g_refs       [2]uint32
	F__g_size       [2]uint32
	F__g1_orig_size uint32
	F__wrefs        uint32
	F__g_signals    [2]uint32
}

type t__tss_t = uint32

type t__thrd_t = uint64

type t__once_flag = struct {
	F__data int32
}

type Tpthread_t = uint64

type Tpthread_mutexattr_t = struct {
	F__align [0]int32
	F__size  [4]int8
}

type Tpthread_condattr_t = struct {
	F__align [0]int32
	F__size  [4]int8
}

type Tpthread_key_t = uint32

type Tpthread_once_t = int32

type Tpthread_attr_t1 = struct {
	F__align [0]int64
	F__size  [56]int8
}

type Tpthread_attr_t = struct {
	F__align [0]int64
	F__size  [56]int8
}

type Tpthread_mutex_t = struct {
	F__size  [0][40]int8
	F__align [0]int64
	F__data  t__pthread_mutex_s
}

type Tpthread_cond_t = struct {
	F__size  [0][48]int8
	F__align [0]int64
	F__data  t__pthread_cond_s
}

type Tpthread_rwlock_t = struct {
	F__size  [0][56]int8
	F__align [0]int64
	F__data  t__pthread_rwlock_arch_t
}

type Tpthread_rwlockattr_t = struct {
	F__align [0]int64
	F__size  [8]int8
}

type Tpthread_spinlock_t = int32

type Tpthread_barrier_t = struct {
	F__align [0]int64
	F__size  [32]int8
}

type Tpthread_barrierattr_t = struct {
	F__align [0]int32
	F__size  [4]int8
}

type Trandom_data = struct {
	Ffptr      uintptr
	Frptr      uintptr
	Fstate     uintptr
	Frand_type int32
	Frand_deg  int32
	Frand_sep  int32
	Fend_ptr   uintptr
}

type Tdrand48_data = struct {
	F__x     [3]uint16
	F__old_x [3]uint16
	F__c     uint16
	F__init  uint16
	F__a     uint64
}

type t__compar_fn_t = uintptr

type Tcomparison_fn_t = uintptr

type t__compar_d_fn_t = uintptr

type Tuint8_t = uint8

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast8_t = int8

type Tint_fast16_t = int64

type Tint_fast32_t = int64

type Tint_fast64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast16_t = uint64

type Tuint_fast32_t = uint64

type Tuint_fast64_t = uint64

type Tintptr_t = int64

type Tuintptr_t = uint64

type Tintmax_t = int64

type Tuintmax_t = uint64

type t__gwchar_t = int32

type Timaxdiv_t = struct {
	Fquot int64
	Frem  int64
}

type TPCRE2_UCHAR8 = uint8

type TPCRE2_UCHAR16 = uint16

type TPCRE2_UCHAR32 = uint32

type TPCRE2_SPTR8 = uintptr

type TPCRE2_SPTR16 = uintptr

type TPCRE2_SPTR32 = uintptr

type Tpcre2_jit_callback_8 = uintptr

type Tpcre2_callout_block_8 = struct {
	Fversion               Tuint32_t
	Fcallout_number        Tuint32_t
	Fcapture_top           Tuint32_t
	Fcapture_last          Tuint32_t
	Foffset_vector         uintptr
	Fmark                  TPCRE2_SPTR8
	Fsubject               TPCRE2_SPTR8
	Fsubject_length        Tsize_t
	Fstart_match           Tsize_t
	Fcurrent_position      Tsize_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR8
	Fcallout_flags         Tuint32_t
	F__ccgo_pad16          [4]byte
}

type Tpcre2_callout_enumerate_block_8 = struct {
	Fversion               Tuint32_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_number        Tuint32_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR8
}

type Tpcre2_substitute_callout_block_8 = struct {
	Fversion        Tuint32_t
	Finput          TPCRE2_SPTR8
	Foutput         TPCRE2_SPTR8
	Foutput_offsets [2]Tsize_t
	Fovector        uintptr
	Foveccount      Tuint32_t
	Fsubscount      Tuint32_t
}

type Tpcre2_jit_callback_16 = uintptr

type Tpcre2_callout_block_16 = struct {
	Fversion               Tuint32_t
	Fcallout_number        Tuint32_t
	Fcapture_top           Tuint32_t
	Fcapture_last          Tuint32_t
	Foffset_vector         uintptr
	Fmark                  TPCRE2_SPTR16
	Fsubject               TPCRE2_SPTR16
	Fsubject_length        Tsize_t
	Fstart_match           Tsize_t
	Fcurrent_position      Tsize_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR16
	Fcallout_flags         Tuint32_t
	F__ccgo_pad16          [4]byte
}

type Tpcre2_callout_enumerate_block_16 = struct {
	Fversion               Tuint32_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_number        Tuint32_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR16
}

type Tpcre2_substitute_callout_block_16 = struct {
	Fversion        Tuint32_t
	Finput          TPCRE2_SPTR16
	Foutput         TPCRE2_SPTR16
	Foutput_offsets [2]Tsize_t
	Fovector        uintptr
	Foveccount      Tuint32_t
	Fsubscount      Tuint32_t
}

type Tpcre2_jit_callback_32 = uintptr

type Tpcre2_callout_block_32 = struct {
	Fversion               Tuint32_t
	Fcallout_number        Tuint32_t
	Fcapture_top           Tuint32_t
	Fcapture_last          Tuint32_t
	Foffset_vector         uintptr
	Fmark                  TPCRE2_SPTR32
	Fsubject               TPCRE2_SPTR32
	Fsubject_length        Tsize_t
	Fstart_match           Tsize_t
	Fcurrent_position      Tsize_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR32
	Fcallout_flags         Tuint32_t
	F__ccgo_pad16          [4]byte
}

type Tpcre2_callout_enumerate_block_32 = struct {
	Fversion               Tuint32_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_number        Tuint32_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR32
}

type Tpcre2_substitute_callout_block_32 = struct {
	Fversion        Tuint32_t
	Finput          TPCRE2_SPTR32
	Foutput         TPCRE2_SPTR32
	Foutput_offsets [2]Tsize_t
	Fovector        uintptr
	Foveccount      Tuint32_t
	Fsubscount      Tuint32_t
}

const _REG_ASSERT = 1
const _REG_BADBR = 2
const _REG_BADPAT = 3
const _REG_BADRPT = 4
const _REG_EBRACE = 5
const _REG_EBRACK = 6
const _REG_ECOLLATE = 7
const _REG_ECTYPE = 8
const _REG_EESCAPE = 9
const _REG_EMPTY = 10
const _REG_EPAREN = 11
const _REG_ERANGE = 12
const _REG_ESIZE = 13
const _REG_ESPACE = 14
const _REG_ESUBREG = 15
const _REG_INVARG = 16
const _REG_NOMATCH = 17

type Tregex_t = struct {
	Fre_pcre2_code uintptr
	Fre_match_data uintptr
	Fre_endp       uintptr
	Fre_nsub       Tsize_t
	Fre_erroffset  Tsize_t
	Fre_cflags     int32
	F__ccgo_pad6   [4]byte
}

type Tregoff_t = int32

type Tregmatch_t = struct {
	Frm_so Tregoff_t
	Frm_eo Tregoff_t
}

/* Debian had a patch that used different names. These are now here to save
them having to maintain their own patch, but are not documented by PCRE2. */

/* End of pcre2posix.h */

/* Table to translate PCRE2 compile time error codes into POSIX error codes.
Only a few PCRE2 errors with a value greater than 23 turn into special POSIX
codes: most go to REG_BADPAT. The second table lists, in pairs, those that
don't. */

var _eint1 = [24]int32{
	1:  _REG_EESCAPE,
	2:  _REG_EESCAPE,
	3:  _REG_EESCAPE,
	4:  _REG_BADBR,
	5:  _REG_BADBR,
	6:  _REG_EBRACK,
	7:  _REG_ECTYPE,
	8:  _REG_ERANGE,
	9:  _REG_BADRPT,
	10: _REG_ASSERT,
	11: _REG_BADPAT,
	12: _REG_BADPAT,
	13: _REG_BADPAT,
	14: _REG_EPAREN,
	15: _REG_ESUBREG,
	16: _REG_INVARG,
	17: _REG_INVARG,
	18: _REG_EPAREN,
	19: _REG_ESIZE,
	20: _REG_ESIZE,
	21: _REG_ESPACE,
	22: _REG_EPAREN,
	23: _REG_ASSERT,
}

var _eint2 = [12]int32{
	0:  int32(30),
	1:  _REG_ECTYPE,
	2:  int32(32),
	3:  _REG_INVARG,
	4:  int32(37),
	5:  _REG_EESCAPE,
	6:  int32(56),
	7:  _REG_INVARG,
	8:  int32(92),
	9:  _REG_INVARG,
	10: int32(99),
	11: _REG_EESCAPE,
}

/* Table of texts corresponding to POSIX error codes */

var _pstring = [18]uintptr{
	0:  __ccgo_ts,
	1:  __ccgo_ts + 1,
	2:  __ccgo_ts + 16,
	3:  __ccgo_ts + 44,
	4:  __ccgo_ts + 58,
	5:  __ccgo_ts + 72,
	6:  __ccgo_ts + 86,
	7:  __ccgo_ts + 100,
	8:  __ccgo_ts + 131,
	9:  __ccgo_ts + 141,
	10: __ccgo_ts + 161,
	11: __ccgo_ts + 178,
	12: __ccgo_ts + 192,
	13: __ccgo_ts + 212,
	14: __ccgo_ts + 231,
	15: __ccgo_ts + 252,
	16: __ccgo_ts + 271,
	17: __ccgo_ts + 284,
}

/*************************************************
*          Translate error code to string        *
*************************************************/
func Xpcre2_regerror(tls *libc.TLS, errcode int32, preg uintptr, errbuf uintptr, errbuf_size Tsize_t) (r Tsize_t) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var message, v1 uintptr
	var used int32
	_, _, _ = message, used, v1
	if errcode <= 0 || errcode >= int32(libc.Uint64FromInt64(144)/libc.Uint64FromInt64(8)) {
		v1 = __ccgo_ts + 297
	} else {
		v1 = _pstring[errcode]
	}
	message = v1
	if preg != libc.UintptrFromInt32(0) && int32((*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset) != -int32(1) {
		used = libc.X__builtin_snprintf(tls, errbuf, errbuf_size, __ccgo_ts+316, libc.VaList(bp+8, message, int32((*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset)))
	} else {
		used = libc.X__builtin_snprintf(tls, errbuf, errbuf_size, __ccgo_ts+334, libc.VaList(bp+8, message))
	}
	return uint64(used + int32(1))
}

/*************************************************
*           Free store held by a regex           *
*************************************************/
func Xpcre2_regfree(tls *libc.TLS, preg uintptr) {
	libpcre2_8.Xpcre2_match_data_free_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data)
	libpcre2_8.Xpcre2_code_free_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code)
}

/*************************************************
*            Compile a regular expression        *
*************************************************/

/*
Arguments:

	preg        points to a structure for recording the compiled expression
	pattern     the pattern to compile
	cflags      compilation flags

Returns:      0 on success

	various non-zero codes on failure
*/
func Xpcre2_regcomp(tls *libc.TLS, preg uintptr, pattern uintptr, cflags int32) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i uint32
	var options int32
	var patlen Tsize_t
	var v1 uint64
	var _ /* erroffset at bp+0 */ Tsize_t
	var _ /* errorcode at bp+8 */ int32
	var _ /* re_nsub at bp+12 */ int32
	_, _, _, _ = i, options, patlen, v1
	options = 0
	*(*int32)(unsafe.Pointer(bp + 12)) = 0
	if cflags&int32(m_REG_PEND) != 0 {
		v1 = uint64(int64((*Tregex_t)(unsafe.Pointer(preg)).Fre_endp) - int64(int64(pattern)))
	} else {
		v1 = ^libc.Uint64FromInt32(0)
	}
	patlen = v1
	if cflags&int32(m_REG_ICASE) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000008))
	}
	if cflags&int32(m_REG_NEWLINE) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000400))
	}
	if cflags&int32(m_REG_DOTALL) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000020))
	}
	if cflags&int32(m_REG_NOSPEC) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x02000000))
	}
	if cflags&int32(m_REG_UTF) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00080000))
	}
	if cflags&int32(m_REG_UCP) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00020000))
	}
	if cflags&int32(m_REG_UNGREEDY) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00040000))
	}
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_cflags = cflags
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code = libpcre2_8.Xpcre2_compile_8(tls, pattern, patlen, uint32(uint32(options)), bp+8, bp, libc.UintptrFromInt32(0))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset = *(*Tsize_t)(unsafe.Pointer(bp))
	if (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code == libc.UintptrFromInt32(0) {
		/* A negative value is a UTF error; otherwise all error codes are greater
		than COMPILE_ERROR_BASE, but check, just in case. */
		if *(*int32)(unsafe.Pointer(bp + 8)) < int32(m_COMPILE_ERROR_BASE) {
			return _REG_BADPAT
		}
		*(*int32)(unsafe.Pointer(bp + 8)) -= int32(m_COMPILE_ERROR_BASE)
		if *(*int32)(unsafe.Pointer(bp + 8)) < int32(libc.Uint64FromInt64(96)/libc.Uint64FromInt64(4)) {
			return _eint1[*(*int32)(unsafe.Pointer(bp + 8))]
		}
		i = uint32(0)
		for {
			if !(uint64(uint64(i)) < libc.Uint64FromInt64(48)/libc.Uint64FromInt64(4)) {
				break
			}
			if *(*int32)(unsafe.Pointer(bp + 8)) == _eint2[i] {
				return _eint2[i+uint32(1)]
			}
			goto _2
		_2:
			i += uint32(2)
		}
		return _REG_BADPAT
	}
	libpcre2_8.Xpcre2_pattern_info_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code, uint32(m_PCRE2_INFO_CAPTURECOUNT), bp+12)
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_nsub = uint64(*(*int32)(unsafe.Pointer(bp + 12)))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data = libpcre2_8.Xpcre2_match_data_create_8(tls, uint32(*(*int32)(unsafe.Pointer(bp + 12))+int32(1)), libc.UintptrFromInt32(0))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset = uint64(-libc.Int32FromInt32(1)) /* No meaning after successful compile */
	if (*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data == libc.UintptrFromInt32(0) {
		/* LCOV_EXCL_START */
		libpcre2_8.Xpcre2_code_free_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code)
		return _REG_ESPACE
		/* LCOV_EXCL_STOP */
	}
	return 0
}

/*************************************************
*              Match a regular expression        *
*************************************************/

/*
	A suitable match_data block, large enough to hold all possible captures, was

obtained when the pattern was compiled, to save having to allocate and free it
for each match. If REG_NOSUB was specified at compile time, the nmatch and
pmatch arguments are ignored, and the only result is yes/no/error.
*/
func Xpcre2_regexec(tls *libc.TLS, preg uintptr, string1 uintptr, nmatch Tsize_t, pmatch uintptr, eflags int32) (r int32) {
	var eo, options, rc, so, v2, v3 int32
	var i Tsize_t
	var md, ovector uintptr
	var v5 Tregoff_t
	_, _, _, _, _, _, _, _, _, _ = eo, i, md, options, ovector, rc, so, v2, v3, v5
	options = 0
	md = (*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data
	if string1 == libc.UintptrFromInt32(0) {
		return _REG_INVARG
	}
	if eflags&int32(m_REG_NOTBOL) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000001))
	}
	if eflags&int32(m_REG_NOTEOL) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000002))
	}
	if eflags&int32(m_REG_NOTEMPTY) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000004))
	}
	/* When REG_NOSUB was specified, or if no vector has been passed in which to
	   put captured strings, ensure that nmatch is zero. This will stop any attempt to
	   write to pmatch. */
	if (*Tregex_t)(unsafe.Pointer(preg)).Fre_cflags&int32(m_REG_NOSUB) != 0 || pmatch == libc.UintptrFromInt32(0) {
		nmatch = uint64(0)
	}
	/* REG_STARTEND is a BSD extension, to allow for non-NUL-terminated strings.
	   The man page from OS X says "REG_STARTEND affects only the location of the
	   string, not how it is matched". That is why the "so" value is used to bump the
	   start location rather than being passed as a PCRE2 "starting offset". */
	if eflags&int32(m_REG_STARTEND) != 0 {
		if pmatch == libc.UintptrFromInt32(0) {
			return _REG_INVARG
		}
		so = (*(*Tregmatch_t)(unsafe.Pointer(pmatch))).Frm_so
		eo = (*(*Tregmatch_t)(unsafe.Pointer(pmatch))).Frm_eo
	} else {
		so = 0
		eo = int32(libc.Xstrlen(tls, string1))
	}
	rc = libpcre2_8.Xpcre2_match_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code, string1+uintptr(so), uint64(eo-so), uint64(0), uint32(uint32(options)), md, libc.UintptrFromInt32(0))
	/* Successful match */
	if rc >= 0 {
		ovector = libpcre2_8.Xpcre2_get_ovector_pointer_8(tls, md)
		if uint64(uint64(rc)) > nmatch {
			rc = int32(int32(nmatch))
		}
		i = uint64(0)
		for {
			if !(i < uint64(uint64(rc))) {
				break
			}
			if *(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2))*8)) == ^libc.Uint64FromInt32(0) {
				v2 = -int32(1)
			} else {
				v2 = int32(*(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2))*8)) + uint64(uint64(so)))
			}
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_so = v2
			if *(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2)+uint64(1))*8)) == ^libc.Uint64FromInt32(0) {
				v3 = -int32(1)
			} else {
				v3 = int32(*(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2)+uint64(1))*8)) + uint64(uint64(so)))
			}
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_eo = v3
			goto _1
		_1:
			i++
		}
		for {
			if !(i < nmatch) {
				break
			}
			v5 = -libc.Int32FromInt32(1)
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_eo = v5
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_so = v5
			goto _4
		_4:
			i++
		}
		return 0
	}
	/* Unsuccessful match */
	if rc <= -int32(3) && rc >= -int32(23) {
		return _REG_INVARG
	}
	/* Most of these are events that won't occur during testing, so exclude them
	   from coverage. */
	switch rc {
	case -int32(63):
		return _REG_ESPACE
	case -int32(1):
		return _REG_NOMATCH
		/* LCOV_EXCL_START */
		fallthrough
	case -int32(32):
		return _REG_INVARG
	case -int32(31):
		return _REG_INVARG
	case -int32(34):
		return _REG_INVARG
	case -int32(36):
		return _REG_INVARG
	case -int32(47):
		return _REG_ESPACE
	case -int32(48):
		return _REG_ESPACE
	case -int32(51):
		return _REG_INVARG
	default:
		return _REG_ASSERT
		/* LCOV_EXCL_STOP */
	}
	return r
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\x00internal error\x00invalid repeat counts in {}\x00pattern error\x00? * + invalid\x00unbalanced {}\x00unbalanced []\x00collation error - not relevant\x00bad class\x00bad escape sequence\x00empty expression\x00unbalanced ()\x00bad range inside []\x00expression too big\x00failed to get memory\x00bad back reference\x00bad argument\x00match failed\x00unknown error code\x00%s at offset %-6d\x00%s\x00"
